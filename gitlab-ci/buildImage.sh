#!/bin/bash

set -e

if [ -z ${1+x} ]; then
    echo "Usage: ./buildImage.sh [branch1, branch2, ...]"
    exit 1
else
    branches=( ${1//,/ } )
    images=( ${2//,/ } )

    export BUILD_DATE=$(date +"%Y.%m.%d")

    for branch in "${branches[@]}"; do
        # Get Shinobi's version
        echo "  - Get Shinobi's version ..."
        wget -q "https://gitlab.com/Shinobi-Systems/Shinobi/raw/${branch}/package.json" -O ./ShinobiPro_package.json
        export APP_VERSION=$( node -pe "require('./ShinobiPro_package.json')['version']" )
        echo "  - Setting APP_VERSION to ${APP_VERSION} ..."

        for image in "${images[@]}"; do
            echo "  - Building image: ${CONTAINER_TEST_IMAGE}-${branch}_${image} ..."
            if [ "${FAKE_CI}" = "true" ]; then
                docker build \
                    -f ./dockerfiles/${image}/Dockerfile \
                    -t $CONTAINER_TEST_IMAGE-${branch}_${image} \
                    --build-arg ARG_APP_VERSION=$APP_VERSION \
                    --build-arg ARG_APP_CHANNEL=$CI_COMMIT_REF_SLUG \
                    --build-arg ARG_APP_COMMIT=$CI_COMMIT_SHA \
                    --build-arg ARG_BUILD_DATE="$BUILD_DATE" \
                    --build-arg ARG_FLAVOR="${image}" \
                    --build-arg ARG_APP_BRANCH="${branch}" .
            else
                docker build --pull \
                    -f ./dockerfiles/${image}/Dockerfile \
                    -t $CONTAINER_TEST_IMAGE-${branch}_${image} \
                    --build-arg ARG_APP_VERSION=$APP_VERSION \
                    --build-arg ARG_APP_CHANNEL=$CI_COMMIT_REF_SLUG \
                    --build-arg ARG_APP_COMMIT=$CI_COMMIT_SHA \
                    --build-arg ARG_BUILD_DATE="$BUILD_DATE" \
                    --build-arg ARG_FLAVOR="${image}" \
                    --build-arg ARG_APP_BRANCH="${branch}" .
            fi

            echo "  - Pushing image to GitLab repository: ${CONTAINER_TEST_IMAGE}-${branch}_${image} ..."
            if [ "${FAKE_CI}" = "true" ]; then
                echo "    fake --> docker push ${CONTAINER_TEST_IMAGE}-${branch}_${image} ..."
            else
                docker push ${CONTAINER_TEST_IMAGE}-${branch}_${image}
            fi
        done
    done
fi
