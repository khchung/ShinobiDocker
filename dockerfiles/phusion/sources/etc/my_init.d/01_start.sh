#!/bin/sh
set -e

# Update Shinobi to latest version on container start?

echo "========================================================================="
echo "-------------------------------------------------------------------------"
echo "        Update Shinobi to latest version on container start?"
echo "-------------------------------------------------------------------------"
if [ "$APP_UPDATE" = "auto" ]; then
    echo "Checking for Shinobi updates ..."
    git reset --hard
    git pull
    npm install
fi
echo "========================================================================="

# Install custom Node JS packages like mqtt, etc.
echo "========================================================================="
echo "-------------------------------------------------------------------------"
echo "        Install additional Node JS packages?"
echo "-------------------------------------------------------------------------"
if [ -n "${ADDITIONAL_NODEJS_PACKAGES}" ]; then
    echo "Install packages: ${ADDITIONAL_NODEJS_PACKAGES} ..."
    npm install ${ADDITIONAL_NODEJS_PACKAGES}
fi
echo "========================================================================="
