#!/bin/sh
set -e

#   Set up database connection

echo "========================================================================="
echo "-------------------------------------------------------------------------"
echo "        Setting up database connection"
echo "-------------------------------------------------------------------------"
# Use embedded SQLite3 database ?
if [ "${EMBEDDEDDB}" = "true" ] || [ "${EMBEDDEDDB}" = "TRUE" ]; then
    echo "Prepare Shinobi to use the embedded SQLite database."
    # Create SQLite3 database if it does not exists
    chmod -R 777 /opt/dbdata

    if [ ! -e "/opt/dbdata/shinobi.sqlite" ]; then
        echo "- Creating shinobi.sqlite for SQLite3..."
        cp /opt/shinobi/sql/shinobi.sample.sqlite /opt/dbdata/shinobi.sqlite
    fi

    # Set database to SQLite3
    echo "Set database type to SQLite3 ..."
    node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" databaseType="sqlite3"
    node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" db.filename="/opt/dbdata/shinobi.sqlite"
    node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" db.user="DELETE"
    node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" db.password="DELETE"
    node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" db.host="DELETE"
    node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" db.port="DELETE"
    node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" db.database="DELETE"
else
    # Set database to SQLite3
    echo "Set database type to MariaDB ..."
    node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" databaseType="DELETE"
    node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" db.filename="DELETE"
    node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" db.user="${MYSQL_USER}"
    node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" db.password="${MYSQL_PASSWORD}"
    node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" db.host="${MYSQL_HOST}"
    node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" db.port="${MYSQL_PORT}"
    node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" db.database="${MYSQL_DATABASE}"

    echo "Prepare Shinobi to use a MariaDB server."
    # Create MariaDB database if it does not exists
    if [ -n "${MYSQL_HOST}" ]; then
        echo -n "- Waiting for connection to MariaDB server on $MYSQL_HOST ."
        while ! mysqladmin ping -h"$MYSQL_HOST" --port="$MYSQL_PORT"; do
            sleep 1
            echo -n "."
        done
        echo " established."
    fi

    # Create MariaDB database if it does not exists
    if [ -n "${MYSQL_ROOT_USER}" ]; then
        if [ -n "${MYSQL_ROOT_PASSWORD}" ]; then
            echo "Setting up MariaDB database if it does not exists ..."

            mkdir -p sql_temp
            cp -f ./sql/framework.sql ./sql_temp
            cp -f ./sql/user.sql ./sql_temp
            cp -f ./sql/FixLdapAuth.sql ./sql_temp

            if [ -n "${MYSQL_DATABASE}" ]; then
                echo " - Set database name: ${MYSQL_DATABASE}"
                sed -i  -e "s/ccio/${MYSQL_DATABASE}/g" \
                    "./sql_temp/framework.sql"
                
                sed -i  -e "s/ccio/${MYSQL_DATABASE}/g" \
                    "./sql_temp/user.sql"

                sed -i  -e "s/ccio/${MYSQL_DATABASE}/g" \
                    "./sql_temp/FixLdapAuth.sql"
            fi

            if [ -n "${MYSQL_ROOT_USER}" ]; then
                if [ -n "${MYSQL_ROOT_PASSWORD}" ]; then
                    sed -i -e "s/majesticflame/${MYSQL_USER}/g" \
                        -e "s/''/'${MYSQL_PASSWORD}'/g" \
                        -e "s/127.0.0.1/%/g" \
                        "./sql_temp/user.sql"
                fi
            fi

            echo "- Create database schema if it does not exists ..."
            mysql -u $MYSQL_ROOT_USER -p$MYSQL_ROOT_PASSWORD -h $MYSQL_HOST --port=$MYSQL_PORT -e "source ./sql_temp/framework.sql" || true

            echo "- Fix user table for LDAP auth issues ..."
            mysql -u $MYSQL_ROOT_USER -p$MYSQL_ROOT_PASSWORD -h $MYSQL_HOST --port=$MYSQL_PORT -e "source ./sql_temp/FixLdapAuth.sql" || true

            echo "- Create database user if it does not exists ..."
            mysql -u $MYSQL_ROOT_USER -p$MYSQL_ROOT_PASSWORD -h $MYSQL_HOST --port=$MYSQL_PORT -e "source ./sql_temp/user.sql" || true

            rm -rf sql_temp
        fi
    fi

    # Set MariaDB configuration from environment variables
    echo "- Set MariaDB configuration from environment variables ..."
    if [ -n "${MYSQL_USER}" ]; then
        echo "  - MariaDB username: ${MYSQL_USER}"
        node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" db.user="${MYSQL_USER}"
    fi

    if [ -n "${MYSQL_PASSWORD}" ]; then
        echo "  - MariaDB password."
        node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" db.password="${MYSQL_PASSWORD}"
    fi

    if [ -n "${MYSQL_HOST}" ]; then
        echo "  - MariaDB server host: ${MYSQL_HOST}"
        node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" db.host="${MYSQL_HOST}"
    fi

    if [ -n "${MYSQL_PORT}" ]; then
        echo "  - MariaDB server port: ${MYSQL_PORT}"
        node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" db.port="${MYSQL_PORT}"
    fi

    if [ -n "${MYSQL_DATABASE}" ]; then
        echo "  - MariaDB database name: ${MYSQL_DATABASE}"
        node /opt/shinobi/tools/modifyJson.js "/opt/shinobi/conf.json" db.database="${MYSQL_DATABASE}"
    fi
fi
echo "========================================================================="
