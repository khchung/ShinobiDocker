#!/bin/sh
set -e

# Create default configurations files from samples if not existing or if empty

echo "========================================================================="
echo "-------------------------------------------------------------------------"
echo "        Setup Shinobi default configuration?"
echo "-------------------------------------------------------------------------"
if [ ! -s /opt/shinobi/conf.json ]; then
    echo "Create default config file /opt/shinobi/conf.json ..."
    cp /opt/shinobi/conf.sample.json /opt/shinobi/conf.json
fi

if [ ! -s /opt/shinobi/super.json ]; then
    echo "Create default config file /opt/shinobi/super.json ..."
    cp /opt/shinobi/super.sample.json /opt/shinobi/super.json
fi

if [ ! -s /opt/shinobi/plugins/motion/conf.json ]; then
    echo "Create default config file /opt/shinobi/plugins/motion/conf.json ..."
    cp /opt/shinobi/plugins/motion/conf.sample.json /opt/shinobi/plugins/motion/conf.json
fi
echo "========================================================================="
